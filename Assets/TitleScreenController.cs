﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScreenController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine("Wait");
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(5);

        SceneManager.LoadScene("01 - Level 01");
    }
}
