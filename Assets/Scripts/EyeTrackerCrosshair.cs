﻿using UnityEngine;
using Tobii.EyeTracking;
using System.Collections;

public class EyeTrackerCrosshair : MonoBehaviour
{
    public Transform target;
    public float speed;

    void Update()
    {
        Vector3 temp = new Vector3();
        Vector2 gazePosition = EyeTracking.GetGazePoint().Screen;
        
        //Debug.Log(" EyeX (" + gazePosition.x + "," + gazePosition.y + ")");

        temp.x = gazePosition.x;
        temp.y = gazePosition.y;
        temp.z = 10f; // Set this to be the distance you want the object to be placed in front of the camera.
        this.transform.position = Camera.main.ScreenToWorldPoint(temp);

        //float step = 25f * Time.deltaTime;
        //transform.position = Vector3.MoveTowards(transform.position, Camera.main.ScreenToWorldPoint(temp), step);
    }
}
