﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    public int damageValue;
    private GameController gameController;
    private float planetLerpProgress;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script!");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Asteroid Collision with: " + other.name);
        if (other.CompareTag("Boundary") || other.CompareTag("Enemy"))
        {
            return;
        }

        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        if (other.CompareTag("Player"))
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }

        if (other.CompareTag("Planet"))
        {
            //Debug.Log("Asteroid Collision with: " + other.name);
            Instantiate(playerExplosion, transform.position, transform.rotation);
            gameController.ReducePlanetHealth(damageValue);
            Destroy(gameObject);

            // Make planet flash red;
            gameController.lerpPlanetColor(other.gameObject);
        }
        else
        {
            gameController.AddScore(scoreValue);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        
    }

    
}
