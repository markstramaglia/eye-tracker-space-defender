﻿using UnityEngine;
using Tobii.EyeTracking;
using System.Collections;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public float tilt;
    public Boundary boundary;
    public Transform eyeTrackerCrosshair;
    private float t; // lerp control variable

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private float nextFire;
    private AudioSource audioSource;

    // Start executes once when the ship instance is initialized
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        nextFire = Time.time + fireRate;
    }

    IEnumerator LerpColor(GameObject focusedObject)
    {
        
        float progress = 0; //This float will serve as the 3rd parameter of the lerp function.
        float increment = 0.02f / 1; //The amount of change to apply.
        while (progress < 1)
        {
            if (null != focusedObject)
            {
                focusedObject.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.Lerp(Color.red, Color.white, progress);
                progress += increment;
                yield return new WaitForSeconds(0.02f);
            }
            else
            {
                break;
            }
        }
    }

    // Update exectes once per frame
    void Update()
    {
        GameObject focusedObject = EyeTracking.GetFocusedObject();
        if (null != focusedObject)
        {
            //Debug.Log("The focused game object is: " + focusedObject.name + " (ID: " + focusedObject.GetInstanceID() + ")" + " (Tag: " + focusedObject.tag + ")");

            if (focusedObject.tag == "Enemy" && Time.time > nextFire)
            {
                // Set Color
                StartCoroutine("LerpColor", focusedObject);

                //Debug.Log("pew pew!");
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
                audioSource.Play();
            }
        }

        // Follow EyeTrackerCrosshair along X-Axis
        float step = speed * Time.deltaTime;
        Vector3 target = new Vector3(
            Mathf.Clamp(eyeTrackerCrosshair.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            transform.position.z
            );

        if (Mathf.Abs(transform.position.x - target.x) > 0.5)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, step);
        }
    }



    // FixedUpdate executes once per frame
    void FixedUpdate()
    {
        /*
        // Old Movement using Keyboard Input
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
        );
        */

        // Ship Tilt
        rb.rotation = Quaternion.Euler(0.0f, 0.0f, eyeTrackerCrosshair.position.x * -tilt);
    }
}
