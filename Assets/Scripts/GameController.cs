﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Tobii.EyeTracking;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public int waveCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public GUIText scoreText;
    public GUIText gameOverText;
    public GUIText planetHealthText;
    public GUIText userPresenceText;
    public GUIText restartingLevelText;

    private bool gameOver;
    private bool restart;
    private bool wonLevel;

    private int waves;
    private int score;
    private int planetHealth;
    private float timeLeft;

    private float planetLerpProgress;

    // Use this for initialization
    void Start ()
    {
        gameOver = false;
        restart = false;
        wonLevel = false;
        gameOverText.text = "";
        planetHealthText.text = "";
        userPresenceText.text = "";
        restartingLevelText.text = "";
        waves = 0;
        score = 0;
        planetHealth = 100;
        planetLerpProgress = 0;
        UpdateScore();
        UpdatePlanetHealth();
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        if (restart)
        {
            timeLeft -= Time.deltaTime;
            restartingLevelText.text = "Restarting in " + Mathf.Round(timeLeft);
            if (timeLeft < 0)
            {
                restartingLevelText.text = "Restarting in 0";
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        if (wonLevel)
        {
            timeLeft -= Time.deltaTime;
            restartingLevelText.text = "Starting Next Level in " + Mathf.Round(timeLeft);
            if (timeLeft < 0)
            {
                restartingLevelText.text = "Starting Next Level in 0";
                int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
                if (SceneManager.GetActiveScene().name == "01 - Level 01")
                {
                    SceneManager.LoadScene("02 - Level 02");
                }
                if (SceneManager.GetActiveScene().name == "02 - Level 02")
                {
                    SceneManager.LoadScene("03 - Level 03");
                }
                if (SceneManager.GetActiveScene().name == "03 - Level 03")
                {
                    SceneManager.LoadScene("04 - Level 04");
                }
                if (SceneManager.GetActiveScene().name == "04 - Level 04")
                {
                    SceneManager.LoadScene("05 - Level 05");
                }
                if (SceneManager.GetActiveScene().name == "05 - Level 05")
                {
                    SceneManager.LoadScene("05 - Level 05");
                }
            }
        }

        // Automatically pause game if user is not present and detected by Eye Tracker;
        UserPresence userPresence = EyeTracking.GetUserPresence();
        if (userPresence.IsUserPresent)
        {
            userPresenceText.text = "";
            Time.timeScale = 1f;
        }
        else
        {
            userPresenceText.text = "Game Paused\nEye Tracker looking for player...";
            Time.timeScale = 0f;
        }
    }
	
	IEnumerator SpawnWaves ()
    {
        yield return new WaitForSeconds(startWait);
        while (waves < waveCount)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restart = true;
                break;
            }
            waves++;
        }

        if (waves == waveCount)
        {
            NextLevel();
        }
	}

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void ReducePlanetHealth(int newHealthValue)
    {
        if (planetHealth > 0)
        {
            planetHealth -= newHealthValue;
        }

        if (planetHealth < 0)
        {
            planetHealth = 0;
        }
        
        UpdatePlanetHealth();

        if (planetHealth <= 0)
        {
            GameOver();
        }
    }

    void UpdatePlanetHealth()
    {
        planetHealthText.text = "Planet Health: " + planetHealth;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
        timeLeft = 5.0f;
    }

    public void NextLevel()
    {
        gameOverText.text = "You Defended the Planet!";
        wonLevel = true;
        timeLeft = 5.0f;
    }

    public void lerpPlanetColor(GameObject focusedObject)
    {

        planetLerpProgress += 0.1f;
        Debug.Log("planetLerpProgress: " + planetLerpProgress);
        focusedObject.transform.GetComponent<Renderer>().material.color = Color.Lerp(Color.white, Color.red, planetLerpProgress);
    }
}
