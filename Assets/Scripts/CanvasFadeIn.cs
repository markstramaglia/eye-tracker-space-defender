﻿using UnityEngine;
using System.Collections;

public class CanvasFadeIn : MonoBehaviour
{
    public CanvasGroup csGrp;

    void Awake()
    {
        csGrp = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        StartCoroutine("FadeIn");
    }

    IEnumerator FadeIn()
    {
        csGrp.alpha = 0;
        float time = 5f;

        while (csGrp.alpha < 1)
        {
            csGrp.alpha += Time.deltaTime / time;
            yield return null;
        }
    }
}