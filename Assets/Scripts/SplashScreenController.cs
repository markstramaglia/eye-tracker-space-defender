﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScreenController : MonoBehaviour {

    public GUIText epilepsyWarningText;

	// Use this for initialization
	void Start ()
    {
        epilepsyWarningText.text =  "WARNING: PHOTOSENSITIVITY/EPILEPSY SEIZURES\n";
        epilepsyWarningText.text += "\n";
        epilepsyWarningText.text += "A very small percentage of individuals may experience epileptic seizures or \n";
        epilepsyWarningText.text += "blackouts when exposed to certain light patterns or flashing lights. \n";
        epilepsyWarningText.text += "\n";
        epilepsyWarningText.text += "Immediately stop playing and consult a doctor if you experience any of these symptoms. \n";
        epilepsyWarningText.text += "Parents should watch for or ask their children about the above symptoms. \n";
        epilepsyWarningText.text += "\n";
        epilepsyWarningText.text += "If you or any of your relatives have a history of seizures or epilepsy, consult a doctor before playing.\n";

        StartCoroutine("Wait");


    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(5);

        SceneManager.LoadScene("00 - Title Screen");
    }
}
